CCFLAGS=--std=gnu++98 -pedantic -Wall -Werror -ggdb3
efficient_frontier: efficient_frontier.o
	g++ -o efficient_frontier $(CCFLAGS) efficient_frontier.o
efficient_frontier.o: efficient_frontier.cpp Asset.hpp 
	g++ -c $(CCFLAGS) -o efficient_frontier.o efficient_frontier.cpp


clean:
	rm -f *.o  *~ efficient_frontier