#include <iostream>
#include <fstream>
#include <Eigen/Dense>
#include <vector>
#include <string>
#include <iomanip>
#include "Asset.hpp"
#include <unistd.h>

using namespace Eigen;
using namespace std;


void split(string &s, vector<string> &v, string &d){
  string::size_type pos1, pos2;
  pos1=0;
  pos2 =s.find(d);
  while(string::npos != pos2){
    v.push_back(s.substr(pos1, pos2));
    pos1 = pos2 +d.size();
    pos2 = s.find(d,pos1);
    }
    if(pos1 != s.length()){
    v.push_back(s.substr(pos1));}}
 Asset* ReadFile(const char*s1, const char* s2){
  ifstream asset,correlation;
  asset.open(s1, ifstream::in);
  correlation.open(s2, ifstream::in);
  if((!asset.is_open()) && (!correlation.is_open())){
      perror("Could not open file");
      exit(EXIT_FAILURE);
    } 
  string line;
  string d =",";
  vector<string> v;
  Asset *Ass = new Asset();
  Ass->n =0;
  int n=0;
  vector<double> ROR;
  vector<double> variance;
  while(getline(asset, line)){
    split(line,v,d);    
    if(v.size() ==0 || v.size() !=3){
    perror("Invalid formats");
    exit(EXIT_FAILURE);
    }
    Ass->type.push_back(v[0]);
    if(atof(v[1].c_str())==0.0 || atof(v[2].c_str())==0){
        perror("Correlation matrix has non-numeric value");//handle non-numeric value
   	    exit(EXIT_FAILURE);
      }
    else{    
    ROR.push_back(atof(v[1].c_str()));
    variance.push_back(atof(v[2].c_str()));}
    n++;
    v.clear();}
    Ass->n =n;
    Ass->variance.resize(1,Ass->n);
    Ass->ROR.resize(1,Ass->n);
    for(int i=0;i<n;i++){
    Ass->variance(0,i) = variance[i];
    Ass->ROR(0,i) = ROR[i];}    
    Ass->covariance.resize(Ass->n,Ass->n);
    Ass->correlation.resize(Ass->n,Ass->n);
    int j=0; 
    while(getline(correlation,line)){
    split(line,v,d);
    if(v.size() ==0 || v.size() !=Ass->n){
      perror("Invalid format");
       exit(EXIT_FAILURE);
    }
    for(size_t i=0; i<Ass->n;i++){
    if(atof(v[i].c_str())==0.0){
        perror("Correlation matrix has non-numeric value");//handle non-numeric value
   	    exit(EXIT_FAILURE);
      }
      else{
        
        Ass->correlation(i,j)=atof(v[i].c_str());
        Ass->covariance(i,j) =Ass->variance(0,i)*Ass->variance(0,j)*Ass->correlation(i,j);
      }
      }
    v.clear();
    j++;} 
    if(j ==0){
    perror("The file is empty");
    exit(EXIT_FAILURE);}
    for(int i=0;i<n;i++){
      for(int j=0; j<n; j++){
        if((fabs(Ass->correlation(i,j)-Ass->correlation(j,i))>0.0001)||(fabs(Ass->correlation(i,j))>1.0001)){
        perror("The correlation is invalid");
        exit(EXIT_FAILURE);}}      
        if(fabs(Ass->correlation(i,i)-1)>0.00001){
        perror("The correlation is invalid");
        exit(EXIT_FAILURE);}
        }
    return Ass;}
    
Asset * UnrestrictedOptimization(Asset * Ass,double l){
int n = Ass->n;
  MatrixXd A;
  A.resize(2,n);
  for(int i=0; i<n;i++){
  A(0,i) = Ass->ROR(0,i);}
  for(int i=0;i<n;i++){
  A(1,i) = 1;}
  MatrixXd L;//L is the left-hand side function
  MatrixXd AT;
  L.resize(n+2,n+2);
  L.setZero();
  AT = A.transpose();
  L.block(0,0,n,n) = Ass->covariance.block(0,0,n,n);
  L.block(n,0,2,n) = A.block(0,0,2,n);  
  L.block(0,n,n,2) = AT.block(0,0,n,2);
  VectorXd R;
  R.resize(n+2);
  R.setZero();
  R[n] =l;
  R[n+1] =1;
  double ROR = R[n]*100;
  VectorXd X(n+2,1);
  X = L.fullPivHouseholderQr().solve(R); 
  Ass->weight.resize(n);
  Ass->weight = X.head(n);
  Ass->volatility =0;
  Ass->PROR = ROR;
  for(int i=0; i<n;i++){
    for(int j=0; j<n;j++){
       Ass->volatility +=Ass->weight[i]*Ass->weight[j]*Ass->covariance(i,j);}
       }
  return Ass;}
Asset * RestrictedOptimization(Asset *Ass,double l){
int n = Ass->n;
  MatrixXd A;
  A.resize(2,n);
  for(int i=0; i<n;i++){
  A(0,i) = Ass->ROR(0,i);}
  for(int i=0;i<n;i++){
  A(1,i) = 1;}
  MatrixXd L;//L is the left-hand side function
  MatrixXd AT;
  L.resize(n+2,n+2);
  L.setZero();
  AT = A.transpose();
  L.block(0,0,n,n) = Ass->covariance.block(0,0,n,n);
  L.block(n,0,2,n) = A.block(0,0,2,n);  
  L.block(0,n,n,2) = AT.block(0,0,n,2);
  VectorXd R;
  R.resize(n+2);
  R.setZero();
  R[n] =l;
  R[n+1] =1;
  double ROR = R[n]*100;
  VectorXd X(n+2,1);
  X = L.fullPivHouseholderQr().solve(R); 
  Ass->weight.resize(n);
  Ass->weight = X.head(n);
  MatrixXd C;
  int k=0;
  MatrixXd L2 =L;
  VectorXd R2 =R;
  VectorXd X2 =X;  
  for(int i=0;;i++){
  int s =0;
  for(int j=0;j<n;j++){ 
    if(X2[j]<0){
    MatrixXd X = MatrixXd::Zero(n,1);
    X(j,0)=1;
    MatrixXd temp =C;
    C.resize(n,k+1);
    if(k==0){
    C << X;}
    else{
    C <<X,temp;
    }
    k++;
    s=1;
       }
    }
    if(s ==0){
    break;}
    
    
   MatrixXd CT = C.transpose();
   L2.resize(n+2+k,n+2+k);
   L2.setZero();
   L2.block(0,0,n,n) = Ass->covariance.block(0,0,n,n);
   L2.block(n,0,2,n) = A.block(0,0,2,n);  
   L2.block(0,n,n,2) = AT.block(0,0,n,2);
   L2.block(0,n+2,n,k) = C.block(0,0,n,k);
   L2.block(n+2,0,k,n) = CT.block(0,0,k,n);
   R2.resize(n+2+k);
   R2.setZero();
   R2[n] =l;
   R2[n+1] =1;
   X2 = L2.fullPivHouseholderQr().solve(R2);
   //cout << L2 <<"\n";
} 
   Ass->weight = X2.head(n);      
  Ass->volatility =0;
  Ass->PROR =ROR;
  for(int i=0; i<n;i++){
    for(int j=0; j<n;j++){
       Ass->volatility +=Ass->weight[i]*Ass->weight[j]*Ass->covariance(i,j);}
       }       
     return Ass;}
     
int main(int argc, char ** argv){
//read data
  if(argc !=3 && argc != 4){
  std::cerr<<"Wrong format of input";
  return EXIT_FAILURE;
  }
  if(argc ==4 && strlen(argv[1]) != 2){
      std::cerr<<"Option size or position is wrong";
      return EXIT_FAILURE;
  }//test the length of option -r
  int opt;
  bool r = false;
  while ((opt = getopt(argc, argv, "r")) != -1){
    switch(opt){
      case 'r':
        r = true;
        break;
      case '?':
        std::cerr<<"Option is wrong";
        return EXIT_FAILURE;
    }
  }    
//optimization matrix
//unrestricted optimization
  if(r == false){
  Asset* Ass = ReadFile(argv[1],argv[2]);
  cout << "ROR, volatility" << "\n";
  for(double l=0.01;l<0.263;l=l+0.01){
  Ass  = UnrestrictedOptimization(Ass,l);
  cout << fixed <<setprecision(1)<<Ass->PROR<<"%" <<", ";
  cout << fixed <<setprecision(2)<<sqrt(Ass->volatility)*100<<"%"<<"\n";}
   delete Ass; }
//restricted optimization
  if(r == true){
  Asset* Ass = ReadFile(argv[2],argv[3]);
  cout << "ROR, volatility" << "\n";
  for(double l=0.01;l<0.263;l=l+0.01){
  Ass  = RestrictedOptimization(Ass,l);
  cout << fixed <<setprecision(1)<<Ass->PROR<<"%" <<", ";
  cout << fixed <<setprecision(2)<<sqrt(Ass->volatility)*100<<"%"<<"\n";}
   delete Ass; 
  }
  
   return EXIT_SUCCESS;
    }
   
  
  

 
