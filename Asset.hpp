#ifndef __ASSET_HPP__
#define __ASSET_HPP__

#include <cstdio>
#include <cstdlib>
#include <string>
#include <cmath>
#include <algorithm>
#include <vector>
#include <iostream>
#include <Eigen/Dense>
using namespace std;
using namespace Eigen;

class Asset{
public:
  size_t n;
  vector<string> type;
  MatrixXd ROR;
  MatrixXd variance;
  VectorXd weight;
  double volatility;
  double PROR;
  MatrixXd correlation;
  MatrixXd covariance;
  
public:
 Asset(){};
 void CovCalculator();
 ~Asset(){};};
 
#endif
